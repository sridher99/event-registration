import React from 'react';
import {
 View,
 Platform,
 Text,
 TextInput,
 StyleSheet,
 Alert,
 ScrollView,
 Picker,
 TouchableHighlight,
} from 'react-native';
import { Checkbox ,Button} from 'react-native-paper';
import { RadioButton } from 'react-native-paper';
import { Appbar } from 'react-native-paper';
import { Subheading } from 'react-native-paper';

export default class register extends React.Component {
 state = {
   firstname: '',
   lastname: '',
   password: '',
   confrimpassword: '',
   email: '',
   phone_number: '',
   language: '',
   value: '',
   checked1: false,
   checked: false,
 };
 handlefirstname = text => {
   this.setState({ firstname: text });
 };
 handlelastname = text => {
   this.setState({ lastname: text });
 };
 handlepassword = text => {
   this.setState({ password: text });
 };
   handleconfirmpassword = text => {
   this.setState({ confirmpassword: text });
 };
 handleemail = text => {
   console.log(this.state.email);
   this.setState({ email: text });
 };
 handlephone_number = text => {
   this.setState({ phone_number: text });
 };
 handlelanguage = text => {
   this.setState({ language: text });
 };
 handlevalue = text => {
   this.setState({ value: text });
 };
 handlesignup = () => {
   const { password, confirmPassword } = this.state;

   if (password !== confirmPassword) {
     alert("Passwords don't match");
   } else {
alert();
   }
 };
 onClickListener = (
   firstname,
   lastname,
   password,
   confirmpassword,
   email,
   phone_number,
   language,
   value,
   checked1
 ) => {
   alert(
     'firstname: ' +
       firstname +
       '\nlastname: ' +lastname+
       '\nPassword: ' +
       password +
       '\nConfirmpassword: ' +
       confirmpassword +
       '\nEmail: ' +
       email +
       '\nPhone: ' +
       phone_number +
       '\ntype:' +
       language +
       '\nvalue:' +
       value +
       '\ncheck' +
       checked1
   );
 };

 render() {
   const { checked1 } = this.state;
   const { checked } = this.state;

   return (
     <View>

     <Appbar.Header style={styles.head}>
           <Appbar.Content title="Event Registration Form" />
         </Appbar.Header>


          
        
       <ScrollView contentContainerStyle={{ height: 780 }}>
       
         
         <TextInput
           style={styles.input}
           placeholder="Firstname"
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handlefirstname}
         />
         <TextInput
           style={styles.input}
           placeholder="Lastname"
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handlelastname}
         />
         <TextInput
           style={styles.input}
           placeholder="Password"
           secureTextEntry={true}
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handlepassword}
         />
         <TextInput
           style={styles.input}
           placeholder="Confirmpassword"
           secureTextEntry={true}
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handleconfirmpassword}
         />
         <TextInput
           style={styles.input}
           placeholder="Email id"
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handleemail}
         />
         <TextInput
           style={styles.input}
           placeholder="Phone Number"
           autoCapitalize="none"
           placeholderTextColor="#54594b"
           onChangeText={this.handlephone_number}
            maxLength={10} 
           keyboardType={'numeric'}
         />
              <View style={styles.flex}>
           <RadioButton.Group
             onValueChange={value => this.setState({ value })}
             value={this.state.value}>
             <View style={styles.flex}>

               <RadioButton value="Female" />
                 <Subheading>Female</Subheading>
             </View>
             <View style={styles.flex}>
             <RadioButton value="Male" />
               <Subheading>Male</Subheading>
               
             </View>
           </RadioButton.Group>
         </View>

         <Picker
           selectedValue={this.state.language}
           onValueChange={(itemValue, itemIndex) =>
             this.setState({ language: itemValue })
           }>
           <Picker.Item label="select" value="" />
           <Picker.Item label="For Myself " value="For myself" />
           <Picker.Item label="To manage my business" value="my business" />
         </Picker>
<View>
<View style={styles.flex}>
<Subheading>I agree with terms and conditions</Subheading>
         <Checkbox
        
           status={checked1 ? 'checked' : 'unchecked'}
           onPress={() => {
             this.setState({ checked1: !checked1 });
           }}
         />
         </View>
         </View>
      <Button
         mode="contained"
         onPress={() => fetch('http://192.168.43.189:3000/sridher', {
 method: 'POST',
 headers: {
   'Content-Type': 'application/json',
 },
 body: JSON.stringify({
firstname:this.state.firstname,
lastname:this.state.lastname,
password:this.state.password,
confirmpassword:this.state.confirmpassword,
email:this.state.email,
phonenumber:this.state.phone_number,
type:this.state.language,
value:this.state.value,
checked1:this.state.checked1

 }),
})
.then(res => Alert.alert("Successfully Registered"))
.catch(err => console.warn(err))
} >SIGN UP</Button>
  <Button
  style={styles.flexx}
          onPress={() => this.onClickListener(this.state.firstname,this.state.lastname,this.state.password,this.state.confirmpassword,this.state.email,this.state.phone_number,this.state.language,this.state.value,this.state.checked1)}>Alert
        </Button>
        
        
       </ScrollView>
               
         

     </View>
   );
 }
}
const styles = StyleSheet.create({
 input: {
   padding: 15,
   height: 55,
   width: 340,
   margin: 10,
   color: 'black',
   fontSize: 18,
   paddingRight: 16,
   paddingLeft: 16,
    borderRadius: 14,
   fontWeight: '500',
   backgroundColor:'#ccc'
 },


 flex: {
   flexDirection: 'row',
   alignContent: 'center',
   justifyContent: 'center',
 },
  flexx: {
backgroundColor:'#FFA500',
 margin: 10,
color:'white',
 },
 head: {
   backgroundColor: 'green',
 },
});